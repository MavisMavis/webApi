'use strict';

const Moment = require('moment');

module.exports = (sequelize, DataTypes) => {
    let Place = sequelize.define('Place', {
        search_date_time: {
            type: DataTypes.DATE,
            get: function () {
                return Moment(this.getDataValue('date')).format('MMMM Do, YYYY');
            }
        },
        title: DataTypes.STRING,
        description: DataTypes.STRING,
        long: DataTypes.STRING,
        lat: DataTypes.STRING,
        query: DataTypes.STRING,
        query_lat: DataTypes.STRING,
        query_long: DataTypes.STRING,
        address: DataTypes.STRING,
        type: DataTypes.STRING
    });

    return Place;
};