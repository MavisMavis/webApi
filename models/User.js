'use strict';

const Moment = require('moment');

module.exports = (sequelize, DataTypes) => {
    let User = sequelize.define('User', {
        name: DataTypes.STRING,
        password: DataTypes.STRING,
        email: DataTypes.STRING
    });

    return User;
};