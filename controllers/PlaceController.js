'use strict';

const Models         = require('../models/');
var passwordHash     = require('password-hash');
var validator        = require('validator');
var jwt              = require('jsonwebtoken'); // used to create, verify and sign JSON Web Tokens
var settings         = require('../settings');
var googleMapsClient = require('@google/maps').createClient({
    key: settings.keys.google_key
});

var formatPlaceData = function(query, queryLat, queryLong, googleData) {
    var results = [];

    for(var i = 0; i < googleData.length; i++) {

        // this format is according to the daatabase structure
        var data = {
            'title': googleData[i].name,
            'description': '',
            'lat': googleData[i].geometry.location.lat,
            'long': googleData[i].geometry.location.lng,
            'query': query,
            'query_lat': queryLat,
            'query_long': queryLong,
            'address': '',
            'type': JSON.stringify(googleData[i].types)
        };

        results.push(data);
    }
    return results;
};

module.exports = {


    search: function(req, res, next) {
         var query = req.body.query;

         console.log(req.body.lat, req.body.long);

         // check database first
        Models.Place.findAll({
            where: {
                query_lat : req.body.lat,
                query_long: req.body.long
            }
        }).then((result) => {

            // places does not exist
            if(result.length <= 0) {
                // find with google

                console.log("---------> Get Data from Google");

                var results = googleMapsClient.placesNearby({
                    language: 'en',
                    location: [req.body.lat,req.body.long],
                    radius: 10000
                },function(err, response) {
                    if (!err) {
                        // Handle response.

                        // format the results
                        var results = formatPlaceData(req.body.query,req.body.lat, req.body.long, response.json.results);

                        // persists google api results in DB
                        for(var i = 0; i < results.length; i++) {
                            Models.Place.create(results[i]); // create each data in database
                        }

                        // return to client side for display
                        res.send(results);

                    } else if (err === 'timeout') {
                        // Handle timeout.
                    } else if (err.json) {
                        // Inspect err.status for more info.
                    } else {
                        // Handle network error.
                    }
                });

                // send response back to user

            } else {
                console.log("---------> Query from LOCAL Database");
                res.send(result);
            }
        });

    },


};
