'use strict';

const Models      = require('../models/');
var passwordHash  = require('password-hash');
var validator     = require('validator');
var jwt           = require('jsonwebtoken'); // used to create, verify and sign JSON Web Tokens
var settings      = require('../settings');

module.exports = {

    /**
     * Create a User
     * @param req
     * @param res
     * @param next
     */
    create: function(req, res, next) {

        if(!validator.isEmpty(req.body.email) && !validator.isEmpty(req.body.email) && !validator.isEmpty(req.body.name) &&
            validator.isEmail(req.body.email)) {

            Models.User.create({
                name: req.body.name,
                password: passwordHash.generate(req.body.password),
                email: req.body.email,
            }).then(function(result) {

                // return as JSON
                res.send({'success': true, 'msg' : 'Successfully registered, you can now login'});
            })

        } else {
            res.send({'success': false, 'msg' : 'Please fill up all fields'});
        }
    },

    /**
     * Display user info
     * @param req
     * @param res
     * @param next
     */
    read: function(req, res, next) {
        var user_id = req.params.id;
        Models.User.findOne({
            where: {
                id : user_id
            }
        })
        .then((result) => {
            if(typeof(result) != 'null' && result != null) {
                res.send({'success': true, 'user': result});
            } else {
                res.send({'success': false, 'msg': 'User not found'});
            }

        });
    },

    /**
     * Update a User
     * @param req
     * @param res
     * @param next
     */
    update: function(req, res, next) {
        var user_id = req.params.id;

        Models.User.findOne({
            where: {
                id : user_id
            }
        })
        .then((user) => {
            if(typeof(user) != 'null' && user != null) {// user exists

                // updates user
                user.update(
                {
                    name: req.body.name,
                    password: passwordHash.generate(req.body.password),
                    email: req.body.email,
                }
                ).then((result) => {
                    res.send({'success': true, 'msg' : 'Successfully updated user'});
                });

            } else {
                res.send({'success': false, 'msg' : 'User not found'});
            }

        });
    },

    /**
     * Destroy a User
     * @param req
     * @param res
     * @param next
     */
    destroy: function(req, res, next) {
        var user_id = req.params.id;

        Models.User.destroy({
            where: {
                id: user_id
            }
        }).then((result) => {
            res.send({'success': true, 'msg' : 'Successfully deleted user'});
        });
    },

    /**
     * User Login
     * @param req
     * @param res
     * @param next
     */
    login: function(req, res, next) {
          // validate
            // validation successful, find this user by email address

            Models.User.findOne({
                    where: {
                        email : req.body.email
                    }
                })
                .then((result) => {

                    // user does not exists
                    if(typeof(result) == 'null' || result == null) {
                        res.send({'success': false, 'msg' : 'User not found'});
                        return; // break
                    }

                    if(passwordHash.verify(req.body.password, result.password)) { // verify user password
                        // if correct

                        // generate token for user
                        var user = {
                            id: result.id,
                            name: result.name,
                            email: result.email
                        };

                        var token = jwt.sign(user, settings.secret, {
                            expiresIn: 1440 // expires in 24 hours
                        });

                        console.log("Generated token for user : " + token);

                        res.send({ 'success': true, 'msg': 'Successfully logged in', 'user': user,  'token' : token });

                    } else {
                        res.send({'success': false, 'msg' : 'Invalid Password'});
                    }
                });
    },


};