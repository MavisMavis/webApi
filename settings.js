module.exports = {
    secret: '305cde', // for jwt uses
    env: process.env.ENV || 'development',

    // Environment-dependent settings
    development: {
        db: {
            dialect: 'sqlite',
            storage: 'database.sqlite'
        }
    },
    production: {
        db: {
            dialect: 'sqlite',
            storage: 'database.sqlite'
        }
    },

    keys: {
        'google_key' : 'AIzaSyCohUo0zV6gMR5h5Xj5Qf2jTq9a7Bt-Utc'
    }
};