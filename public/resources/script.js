var webApp = angular.module('webApp', ['ngRoute']);
const DEFAULT_ERROR_MSG = "Something went wrong, try again later";

function ucfirst(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}
// configure routes
webApp.config(function($routeProvider) {
    $routeProvider

    // route for the home page
        .when('/', {
            templateUrl : 'pages/home.html',
            controller  : 'mainController'
        })

        // route for the login page
        .when('/login', {
            templateUrl : 'pages/login.html',
            controller  : 'loginController'
        })

        // route for the register page
        .when('/register', {
            templateUrl : 'pages/register.html',
            controller  : 'registerController'
        })

});

// global controller
webApp.controller('globalController', function($scope, $location) {

    $scope.error = false;
    $scope.errorMsg = '';

    $scope.user = (sessionStorage.user)? JSON.parse(sessionStorage.user) : {};
    $scope.token = (sessionStorage.accessToken)? sessionStorage.accessToken : '';

    console.log("user credentials", $scope.user, $scope.token);

    $scope.loggedIn = (!angular.equals($scope.user, {}) && $scope.token.length > 0);

    $scope.goNext = function (hash) {
        $location.path(hash);
    };

    $scope.logout = function () {
        sessionStorage.removeItem('user');
        sessionStorage.removeItem('accessToken');

        $scope.loggedIn = false; // logout
    }

});

// main controller
webApp.controller('mainController', function($scope, $location, $http, $window) {

    $scope.searching = false;

    $scope.searchResults = [];

    $scope.parseJSON = function(strObj){
        return JSON.parse(strObj);
    };

    $scope.formatType = function(type) { // format places type returned by Google
        return ucfirst(type.replace(/_/g, " "));
    };


    /**
     * Google Maps
     */
    $window.map = new google.maps.Map(document.getElementById('map'), {
        center: {
            lat: 4.1279329,
            lng: 105.1214892
        },
        zoom: 6
    });
    var infowindow = new google.maps.InfoWindow();
    var infowindowContent = document.getElementById('infowindow-content');

    var places_markers = []; // for individual markers

    infowindow.setContent(infowindowContent);

    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });

    var input = document.getElementById('pac-input');
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.bindTo('bounds', map);

    /**
     * On user search query change
     */
    autocomplete.addListener('place_changed', function() {

        infowindow.close();
        marker.setVisible(false);

        var place = autocomplete.getPlace();
        var place_marker;

        if (!place.geometry) {
            window.alert("No details available for input: '" + place.name + "'");
            return;
        }

        // If the place has a geometry, then present it on a map.
        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);  // Why 17? Because it looks good.
        }
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);

        var address = '';
        if (place.address_components) {
            address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
            ].join(' ');
        }

        infowindowContent.children['place-name'].textContent = place.name;
        infowindowContent.children['place-address'].textContent = address;
        infowindow.open(map, marker);

        if($scope.$parent.token.length > 0) {

            $scope.searching = true;

            /**
             * Get Data from API Server(backend)
             */
            $http({

                method:'POST',
                url: 'search',
                headers: {'Content-Type':'application/json', 'Authorization': 'Bearer ' + $scope.$parent.token},
                data: {query: place.name, lat: place.geometry.location.lat(), long: place.geometry.location.lng()}

            }).then(function successCallback(response) {

                console.log(response);

                $scope.searchResults = response.data;

                // add markers for new places
                for(var i = 0; i < $scope.searchResults.length; i++) {
                    place_marker = new google.maps.Marker({
                        position: new google.maps.LatLng(response.data[i].lat, response.data[i].long), // from API data
                        map: map,
                        icon: 'http://maps.google.com/mapfiles/ms/icons/blue.png',
                        isActive: false
                    });


                    // when place marker is clicked(focus)
                    google.maps.event.addListener(place_marker, 'click', function() {
                        if(this.isActive) { // make it inactive, blue
                            this.setIcon('http://maps.google.com/mapfiles/ms/icons/blue.png');
                        } else { // make it active

                            // other marker loose focus
                            for(var m = 0; m < places_markers.length; m++) {
                                places_markers[m].setIcon('http://maps.google.com/mapfiles/ms/icons/blue.png');
                                places_markers[m].isActive = false;
                            }

                            this.setIcon('http://maps.google.com/mapfiles/ms/icons/red.png');

                            // zoom to marker
                            map.setZoom(13);
                            map.panTo(this.position);
                        }

                        this.isActive = !this.isActive; // toggle

                    });

                    places_markers.push(place_marker);
                }


                $scope.searching = false;

            }, function errorCallback(response) {
               if(response.status == 401) {
                   alert('Your session has expired, please re-login');

                   // log user out
                   $scope.$parent.loggedIn = false;
                   $scope.$parent.user = {};
                   $scope.$parent.token = '';

                   // redirect to login
                   $location.path('/login');

               }
            });

        } else {
            console.log('No token supplied')
        }

    });


    // when click result, focus the marker
    $scope.clickResult = function(index) {
        new google.maps.event.trigger(places_markers[index], 'click');
    };

});


/**
 * Authentication Controllers
 */

// Login
webApp.controller('loginController', function($scope, $http, $location) {
    $scope.email = '';
    $scope.password = '';
    $scope.$parent.error = false;

    $scope.doLogin = function()
    {
        $http({
            method:'POST',
            url: 'user/login',
            header: {'Content-Type':'application/json'},
            data: {email: $scope.email, password: $scope.password}
        }).then(function successCallback(response) {


            if(response.data.success) { // login successful

                // store access token and user info
                sessionStorage.accessToken = response.data.token;
                sessionStorage.user = JSON.stringify(response.data.user);

                $scope.$parent.token = response.data.token;
                $scope.$parent.user = response.data.user;

                $scope.$parent.loggedIn = true;

                // redirect to home
                $location.path('/');

            } else {
                $scope.$parent.error = true;
                $scope.$parent.errorMsg = response.data.msg;
            }

        }, function errorCallback(response) {
            $scope.$parent.error = true;
            $scope.$parent.errorMsg = DEFAULT_ERROR_MSG;
        });

    };

});

// Register
webApp.controller('registerController', function($scope, $http, $location) {
    $scope.email = '';
    $scope.name = '';
    $scope.password = '';
    $scope.$parent.error = false;

    $scope.doRegister = function() {
        $http({
            method : 'POST',
            url: 'user/create',
            header: {'Content-Type': 'application/json'},
            data: {email: $scope.email, password: $scope.password, name: $scope.name}
        }).then(function successCallback(response) {



            if(response.data.success) {
                // redirect to home
                alert(response.data.msg); // show success msg
                $scope.$parent.loggedIn = false;

                $location.path('/login');

            } else {
                $scope.$parent.error = true;
                $scope.$parent.errorMsg = response.data.msg;
            }

        }, function errorCallback(response) {
            $scope.$parent.error = true;
            $scope.$parent.errorMsg = DEFAULT_ERROR_MSG;
        });
    };
});
