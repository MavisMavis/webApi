var restify     = require('restify'); // RESTful API package
var fs          = require('fs'); // file system
var serveStatic = require('serve-static-restify');  // to server Angular JS Files in Public/
var jwt         = require('restify-jwt'); // for user authentication before able to send API requests
const Settings = require('./settings');
var port        = process.env.PORT || 8080;
// Controllers
const UserController = require('./controllers/UserController');
const PlaceController = require('./controllers/PlaceController');

// Models
const Models = require('./models/');

// create a restify server
var server = restify.createServer({
  name: 'TourAdvisor',
});

server.pre(serveStatic('public/', {'index': ['index.html']})); // for serving Angular JS files

/**
 * Routes and its handlers
 */
server.use(restify.plugins.bodyParser()); // for parsing POST data

// User Routes
server.post('user/create', UserController.create);
server.post('user/login', UserController.login);
server.get('user/:id', jwt({secret: Settings.secret}),   UserController.read);
server.del('user/:id', jwt({secret: Settings.secret}),   UserController.destroy); // delete
server.put('user/:id', jwt({secret: Settings.secret}),   UserController.update); // update

// Search
server.post('search', jwt({secret: Settings.secret}),  PlaceController.search);

server .get('/protected', jwt({secret: Settings.secret}), function(req, res) {
    res.send("Authorized");
});


/**
 * Start Server
 */
Models.sequelize.sync().then(() => { // sequalize sync to sync the sqlite db with the current existing Models

    server.listen(port, function () {
        console.log('API Server listing on port 8080');
    });

});